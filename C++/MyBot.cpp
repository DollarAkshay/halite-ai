#include <math.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <map>
#include <set>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include "hlt.hpp"
#include "networking.hpp"

using namespace std;

#define sp system("pause")
#define FOR(i,a,b) for(int i=a;i<=b;++i)
#define FORD(i,a,b) for(int i=a;i>=b;--i)
#define REP(i,n) FOR(i,0,(int)n-1)
#define pb(x) push_back(x)
#define mp(a,b) make_pair(a,b)
#define DB(format,...) fprintf(logfile, format, ##__VA_ARGS__)
#define MS(a,x) memset(a,x,sizeof(a))
#define SORT(a,n) sort(begin(a),begin(a)+n)
#define REV(a,n) reverse(begin(a),begin(a)+n)
#define ll long long
#define pii pair<int,int>
#define MOD 1000000007

#define PRODUCTION_FACTOR 5
#define SELF_PATH_COST 10

#define S_RANDOM 0
#define S_TUNNELING 1
#define S_GENERAL 2

class Compare{
    public:
    bool operator()(const pair<hlt::Location, int> &a,const pair<hlt::Location, int> &b){
        return a.second > b.second;
    }
};


unsigned char myID, totalPlayers = 2;
int state, tunnelSize, tunnelBotPos;
int turn, maxTurns;
double maxTunnelVal, tunnelTime;
clock_t mainClock, stopwatch;
FILE *logfile;
hlt::GameMap gamemap;

int territory[10], production[10], strength[10];
vector<hlt::Location> tunnel, tempTunnel, border;

set<hlt::Move> moves;

char direction[100][100], visited[100][100];
int cost[100][100];
double PSRatio[100][100];
double heuristicsVal[100][100];
hlt::Location parent[100][100];


void calculateTPS(){

    //Calculate Territory, Production and Strength
    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            hlt::Site cell = gamemap.getSite(hlt::Location(x, y));
            int owner = cell.owner;
            territory[owner]++;
            production[owner] += cell.production;
            strength[owner] += cell.strength;
        }
    }

}

hlt::Location calculateHeuristics(){

    double maxVal = -1;
    hlt::Location maxLoc;

    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            hlt::Site cell = gamemap.getSite( hlt::Location(x, y));
            int prod = cell.production;
            int dist = cost[y][x];

            double turnFactor = (double) turn/maxTurns;

            if(cell.owner == myID){
                heuristicsVal[y][x] = 0;
            }
            else if(cell.owner == 0){
                heuristicsVal[y][x] =  10000*pow(prod, 2 - turnFactor/0.8 ) / pow(dist, 2 - turnFactor) ;
            }
            else{
                heuristicsVal[y][x] =  10000*pow(prod, 1.5 - turnFactor ) / pow(dist, 2 - turnFactor);
            }

            if(heuristicsVal[y][x] > maxVal){
                maxVal = heuristicsVal[y][x];
                maxLoc = hlt::Location(x, y);
            }

        }
    }

    return maxLoc;

}

void printQueue(priority_queue< pair<hlt::Location, int>, vector<pair<hlt::Location, int> >, Compare> pq){

    DB("The Queue is : ");
    while( !pq.empty() ){
        pair<hlt::Location, int> node = pq.top();
        pq.pop();
        DB("%d ", node.second);
    }
    DB("\n");

}

void dijkstra(hlt::Location src){

    priority_queue< pair<hlt::Location, int>, vector<pair<hlt::Location, int> >, Compare > pq;

    REP(y, gamemap.height){
        REP(x, gamemap.width){
            cost[y][x] = 1000000;
            parent[y][x] = hlt::Location(-1, -1);
        }
    }

    pq.push( mp(src, 0) );
    cost[src.y][src.x] = 0;
    parent[src.y][src.x] = hlt::Location(-1, -1);

    while( !pq.empty() ){
        pair<hlt::Location, int> node = pq.top();
        pq.pop();

        hlt::Location l = node.first;

        FOR(dir, 1, 4){
            hlt::Location n = gamemap.getLocation(l, dir);
            if( cost[n.y][n.x] ==  1000000){

                if(gamemap.getSite(n).owner == myID )
                    cost[n.y][n.x] = cost[l.y][l.x] + SELF_PATH_COST;
                else
                    cost[n.y][n.x] = gamemap.getSite(n).strength + cost[l.y][l.x];

                parent[n.y][n.x] = l;
                direction[n.y][n.x] = dir;
                pq.push( mp(n, cost[n.y][n.x]));
            }
        }

    }

}

void debugInfo(){

    DB("Production to Strength Ratio\n   ");
    REP(x, gamemap.width)
        DB("%2d ", (int) x );
    DB("\n");
    REP(y, gamemap.height) {
        DB("%2d ", y);
        REP(x, gamemap.width) {
            DB("%2d ", (int) round(PSRatio[y][x]) );
        }
        DB("\n");
    }

    DB("\n\nTime taken to find Tunnel : %.4f\n", tunnelTime);
    DB("Tunnel value : %.4f\n", maxTunnelVal);
    DB("Size of tunnel : %d\n", tunnelSize);
    DB("The tunnel is : \n");
    REP(i, tunnel.size()){
        DB("%2d, %2d\n", tunnel[i].x, tunnel[i].y );
    }

    DB("\n\nCost from Start\n");
    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            DB("%6d ",  cost[y][x]);
        }
        DB("\n");
    }


    DB("\n\nHeuristics Value\n");
    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            DB("%3.0f ",  heuristicsVal[y][x]);
        }
        DB("\n");
    }

}

void findTunnel(hlt::Location cur, int curVal, int len){

    tempTunnel.push_back(cur);
    visited[cur.y][cur.x] = 1;

    if(len==0){
        if(curVal > maxTunnelVal){
            tunnel.clear();
            tunnel = tempTunnel;
            maxTunnelVal = curVal;
        }
    }
    else{
        FOR(dir, 1, 4){
            hlt::Location neigh = gamemap.getLocation(cur, dir);
            if(visited[neigh.y][neigh.x] == 0){
                findTunnel(neigh, curVal + PSRatio[neigh.y][neigh.x]*(1 + len/(2*tunnelSize))   , len - 1);
            }
        }
    }

    tempTunnel.pop_back();
    visited[cur.y][cur.x] = 0;

}

void init(){

    srand(time(NULL));
    cout.sync_with_stdio(0);
    logfile = fopen("log.txt", "w");
    DB("\n\n --- INITIALIZE --- \n");
    getInit(myID, gamemap);

    state = 1;
    maxTurns = 10 * sqrt(gamemap.width * gamemap.height); 
    hlt::Location start;

    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            double prod = gamemap.getSite(hlt::Location(x, y)).production+0.1;
            double str = gamemap.getSite(hlt::Location(x, y)).strength+0.1;
            PSRatio[y][x] = (double)20*pow(prod, 1)/pow(str, 1);
            totalPlayers = max( totalPlayers, gamemap.getSite(hlt::Location(x, y)).owner);
            if (gamemap.getSite(hlt::Location(x, y)).owner == myID){
                start = hlt::Location(x, y);
            }
        }
    }

    clock_t t = clock();

    tunnelSize = min(15, (int)round(8 + ((double)gamemap.width-20)/4 - (totalPlayers-1)) ); 

    findTunnel(start, 0, tunnelSize);
    tunnelTime = ((double)clock() - t)/(CLOCKS_PER_SEC);

    dijkstra(start);
    calculateHeuristics();
    debugInfo();

}

void findShortestDirectionsTo(hlt::Location src){

    priority_queue< pair<hlt::Location, int>, vector<pair<hlt::Location, int> >, Compare > pq;

    REP(y, gamemap.height){
        REP(x, gamemap.width){
            direction[y][x] = 0;
        }
    }
    pq.push( mp(src, 0) );
    direction[src.y][src.x] = -1;

    while( !pq.empty() ){
        pair<hlt::Location, int> node = pq.top();
        pq.pop();

        hlt::Location l = node.first;
        FOR(dir, 1, 4){
            hlt::Location n = gamemap.getLocation(l, dir);
            if( direction[n.y][n.x] == 0 ){
                direction[n.y][n.x] = (dir+2)%4;
                if(direction[n.y][n.x]==0)
                    direction[n.y][n.x] = 4;
                if(gamemap.getSite(n).owner==myID)
                    pq.push( mp(n, SELF_PATH_COST + node.second));
                else
                    pq.push( mp(n, gamemap.getSite(n).strength + node.second));
            }
        }

    }

}

double calcDamageTaken(hlt::Location loc, int pieceStrength){

    double res = 0;
    FOR(dir, 1, 4){
        hlt::Location neigh = gamemap.getLocation(loc, dir);
        int ownerID = gamemap.getSite(neigh).owner;
        if(ownerID!=0 && ownerID!=myID){
            if(gamemap.getSite(neigh).strength == 0)
                res+=0.001;
            else
                res += gamemap.getSite(neigh).strength;
        }
    }

    res = min(res, (double) pieceStrength);
    return res;

}

int calcDamageDone(hlt::Location loc, int pieceStrength){

    int res = 0;
    FOR(dir, 1, 4){
        hlt::Location neigh = gamemap.getLocation(loc, dir);
        int ownerID = gamemap.getSite(neigh).owner;
        if(ownerID!=0 && ownerID!=myID){
            if(gamemap.getSite(neigh).strength == 0)
                res+=0.1;
            else
                res += min((double)pieceStrength, (double)gamemap.getSite(neigh).strength );
        }
    }

    return res;

}

vector<int> getPathTo(hlt::Location src, hlt::Location dest){

    vector<int> path;

    hlt::Location cur = dest;

    while(cur != src){
        int dir = hlt::oppositeDirection(direction[cur.y][cur.x]);
        cur = gamemap.getLocation(cur, dir);
        path.push_back(dir);
    }

    reverse(path.begin(), path.end());
    REP(i, path.size())
        path[i] = hlt::oppositeDirection(path[i]);

    return path;

}

void randomMove(){

    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            if (gamemap.getSite(hlt::Location(x, y)).owner == myID) {
                moves.insert( hlt::Move( hlt::Location(x, y) ,rand()%5) );
            }
        }
    }

}

bool isBorderCell(hlt::Location loc){

    FOR(dir, 1, 4){
        if( gamemap.getSite(loc, dir).owner != myID )
            return true;
    }

    return false;

}

void findAllBorderCells(){

    border.clear();

    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            hlt::Location cur = hlt::Location(x, y);
            if(isBorderCell(cur)){
                border.push_back(cur);
            }
        }
    }

}

void moveToBestHeuristic(hlt::Location cur){

    dijkstra(cur);
    hlt::Location dest = calculateHeuristics();
    vector<int> path = getPathTo(cur, dest);
    hlt::Site cell = gamemap.getSite(cur);
    hlt::Site neigh = gamemap.getSite(cur, path[0]);

    if( neigh.owner != myID ){
        if(neigh.strength <= cell.strength )
            moves.insert( hlt::Move( cur, path[0]) );
        else
            moves.insert( hlt::Move( cur, 0) );
    }
    else {
        if( cell.strength > cell.production*PRODUCTION_FACTOR )
            moves.insert( hlt::Move( cur, path[0]) );
        else
            moves.insert( hlt::Move( cur, 0) );

    }

}

void moveToClosestBorder(hlt::Location cur){

    hlt::Site cell = gamemap.getSite(cur);

    if( cell.strength > cell.production*PRODUCTION_FACTOR ){

        hlt::Location dest = hlt::Location(0, 0);
        int bestDist = 10000;

        REP(i, border.size()){
            int dist = gamemap.getDistance(border[i], cur);
            if(dist < bestDist){
                dest = border[i];
                bestDist = dist;
            }
        }

        int dir = gamemap.getDirection(cur, dest);
        moves.insert( hlt::Move( cur , dir) );

    }
    else{
        moves.insert( hlt::Move( cur , 0) );
    }

}

void generalMove(){

    REP(y, gamemap.height) {
        REP(x, gamemap.width) {
            hlt::Location cur = hlt::Location(x, y);
            hlt::Site cell = gamemap.getSite(cur);

            if (cell.owner == myID) {

                int maxDir = 0;
                double maxPSVal = 0, maxADRatio = 0;

                // Attack Enemy
                FOR(dir, 1, 4){
                    hlt::Location neigh = gamemap.getLocation(cur, dir);
                    double ADRatio = calcDamageDone(neigh, cell.strength)/calcDamageTaken(neigh, cell.strength);
                    if(ADRatio>maxADRatio){
                        maxDir = dir;
                        maxADRatio = ADRatio;
                    }
                }

                if(maxADRatio >= 1){
                    moves.insert( hlt::Move( cur, maxDir) );
                }
                else if(isBorderCell(cur)){
                    moveToBestHeuristic(cur);
                }
                else{
                    moveToBestHeuristic(cur);
                }

            }
        }
    }

}

void tunneling(){

    int posNext = -1;
    hlt::Location next;

    REP(i, tunnel.size()){
        if( gamemap.getSite(tunnel[i]).owner != myID ){
            next = tunnel[i];
            posNext = i;
            break;
        }
    }

    if(posNext==-1){
        state = 2;
        generalMove();
        return;
    }

    tunnelBotPos = posNext-1;
    int neededStrength = gamemap.getSite(tunnel[posNext]).strength;

    while(neededStrength > 0 && tunnelBotPos >= 0){
        neededStrength -= gamemap.getSite(tunnel[tunnelBotPos]).strength ;
        FOR(i, tunnelBotPos, posNext-1){
            neededStrength -= gamemap.getSite(tunnel[i]).production * (i-tunnelBotPos);
        }
        if(neededStrength < 0)
            break;
        tunnelBotPos--;
    }
    DB("Next Tunnel Cell = %2d, %2d\n", next.x, next.y);

    if(tunnelBotPos >= 0 ){
        int dir = gamemap.getDirection(tunnel[tunnelBotPos], tunnel[tunnelBotPos+1] );
        moves.insert( hlt::Move( tunnel[tunnelBotPos] , dir) );

        findShortestDirectionsTo(tunnel[posNext]);

        FORD(i, tunnelBotPos-1, 0){
            hlt::Site cell = gamemap.getSite(tunnel[i]);
            if( cell.strength > cell.production*PRODUCTION_FACTOR ){
                moves.insert( hlt::Move( tunnel[i] , direction[tunnel[i].y][tunnel[i].x]) );
            }
            else{
                 moves.insert( hlt::Move( tunnel[i] , 0) );
            }
        }
        
        tunnelBotPos++;
    }

}

int main() {

    mainClock = clock();
    stopwatch = clock();
    init();
    sendInit("Smart Tunnel Bot");
    DB("Time taken to initialize : %.4f\n", (double)(clock()-stopwatch)/CLOCKS_PER_SEC );
    fflush(logfile);

    while(true) {

        moves.clear();
        getFrame(gamemap);
        turn++;

        stopwatch = clock();
        DB("\n\n --- TURN #%3d--- \n", turn);

        if( turn >= 0.2*maxTurns && state==S_TUNNELING){
            state = S_GENERAL;
        }

        calculateTPS();

        if( state == S_TUNNELING ){
            tunneling();
        }
        else if(state == S_GENERAL){
            generalMove();
        }
        else{
            randomMove();
        }

        sendFrame(moves);

        DB("Time Taken for turn #%d : %.4f\n", turn, (double)(clock()-stopwatch)/CLOCKS_PER_SEC );
        DB("Total Time Taken : %.4f\n", (double)(clock()-mainClock)/CLOCKS_PER_SEC );
        fflush(logfile);
        
    }

    fclose(logfile);
    return 0;
}
