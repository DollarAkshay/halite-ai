#!/bin/bash
cd $(dirname $0)

g++ -std=c++11 -w MyBot.cpp -o MyBot.o
g++ -std=c++11 -w RandomBot.cpp -o RandomBot.o

./halite -d "30 30" "./MyBot.o" "./RandomBot.o" -t


rm RandomBot.o
rm MyBot.o
